import * as connections from '../config/connection';
import { Schema, Document } from 'mongoose';

import { IUser } from "../interfaces/user";
import { UserSchema } from '../schemas/user';

export interface IUserModel extends IUser, Document {
  //custom methods for your model would be defined here
}


export default connections.db.model < IUserModel >('UserModel', UserSchema);