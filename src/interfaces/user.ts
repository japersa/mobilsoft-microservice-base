export interface IUser {
    createdAt ? : Date;
    updatedAt ? : Date;
    name: string;
    email: string;
}