import { Schema } from "mongoose";
import { IUser } from "../interfaces/user";

export const UserSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    }
  },
  {
    collection: "usermodel",
    versionKey: false
  }
).pre("save", next => {
  if (this._doc) {
    const doc: IUser = this._doc;
    const now: Date = new Date();

    if (!doc.createdAt) {
      doc.createdAt = now;
    }
    doc.updatedAt = now;
  }
  next();

  return this;
});
