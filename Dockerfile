FROM node:10
WORKDIR /app
COPY package.json /app
RUN npm install
COPY /dist /app
CMD node index.js
EXPOSE 4300

