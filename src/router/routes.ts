import * as express from 'express';
import UserRouter from './users';
import { IServer } from '../interfaces/server-interface';
import { publicRouter } from './public';
import { loginRouter } from './login';
import { protectedRouter } from './protected';

export default class Routes {
    /**
     * @param  {IServer} server
     * @returns void
     */
    static init(server: IServer): void {
        const router: express.Router = express.Router();

        server.app.use('/', router);
        server.app.use('/api/users', new UserRouter().router);
        server.app.use('/api/public', publicRouter);
        server.app.use('/auth', loginRouter);


    }
}