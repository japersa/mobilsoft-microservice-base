import { pbkdf2, randomBytes } from "crypto";
import { NextFunction, Request, Response, Router } from "express";
import { sign } from "jsonwebtoken";
import { digest, length, secret } from "../config/config";

const loginRouter: Router = Router();

const user = {
  hashedPassword: "236cbd692aa67402b85cf16ba9d2e28e3f291c5381c293a5ca9ca1db1d9ddd6bcc0e1a27f3c33577f560d6b66fe272fa6576fe46ff7b2c4fd771f85a8844aa1dad73ea6021e96add03e1d05516f5992061f63779031a0c76e64227e6eb9c7e62e004f0e75c26ba81efd0371178b022712933815c9adc48681dea85e1590c6566",
  salt: "mhJGub7Wj7mYKVsUdNW0E7hws+FWY3/xk2/gIJqPiumwqIrPSFrPjmPo5uA2OkNXl6u2HchVyium57do0aksnFbM27YSqQ9KAT64+yShc7cEi8xHxPML6i+C8FuPGn7G8Q/0Ib83HOaPBK5hLWTjbqvN09wTIchDrzO934ufhfQ=",
  username: "john",
};

loginRouter.post("/signup", (request: Request, response: Response, next: NextFunction) => {
  if (!request.body.hasOwnProperty("password")) {
    const err = new Error("No password");
    return next(err);
  }

  const salt = randomBytes(128).toString("base64");

  pbkdf2(request.body.password, salt, 10000, length, digest, (err: Error, hash: Buffer) => {
    response.json({
      hashed: hash.toString("hex"),
      salt,
    });
  });
});

// login method
loginRouter.post("/", (request: Request, response: Response, next: NextFunction) => {

  pbkdf2(request.body.password, user.salt, 10000, length, digest, (err: Error, hash: Buffer) => {
    if (err) {
      throw new Error(err.message);
    }

    // check if password is active
    if (hash.toString("hex") !== user.hashedPassword) {
      return response.json({ message: "Wrong password" });
    }

    const toSign = Object.assign({}, { user: user.username, permissions: [] });
    const token = sign(toSign, secret, { expiresIn: "7d" });

    return response.json({ jwt: token });

  });
});

export { loginRouter };
